package context;

import util.Sentence;
import util.SentenceTreeSet;

public enum Context {
	INSTANCE;
	private String rawSentence = null;
	private String taggedSentence = null;
	private SentenceTreeSet<Sentence> sentencesWithProbabilities = new SentenceTreeSet<Sentence>();
	
	public void setCurrentSentence(String currentSentence) {
		this.rawSentence = currentSentence;
	}
	
	public void setTaggedSentence(String taggedSentence) {
		this.taggedSentence = taggedSentence;
	}
	
	public void addSentencesWithProbabilites(SentenceTreeSet<Sentence> sentencesToAdd) {
		sentencesWithProbabilities.addAll(sentencesToAdd);
	}
	
	public String getCurrentSentence() {
		return rawSentence;
	}
	
	public String getTaggedSentence() {
		return taggedSentence;
	}
	
	public SentenceTreeSet<Sentence> getSentencesWithProbabilities() {
		sentencesWithProbabilities.updateAll();
		return sentencesWithProbabilities;
	}
	
	public void close() {
		this.rawSentence = null;
		this.taggedSentence = null;
		this.sentencesWithProbabilities = new SentenceTreeSet<Sentence>();
	}

}
