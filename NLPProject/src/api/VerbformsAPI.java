package api;

/**
 * Created by ni9174 on 10.11.2015.
 */

import java.util.LinkedList;

import simplenlg.features.Feature;
import simplenlg.features.NumberAgreement;
import simplenlg.features.Person;
import simplenlg.features.Tense;
import simplenlg.framework.NLGFactory;
import simplenlg.lexicon.Lexicon;
import simplenlg.phrasespec.VPPhraseSpec;
import simplenlg.realiser.english.Realiser;

public class VerbformsAPI {

	public static LinkedList<String> getOtherForms(String verb, String type){
		Lexicon lexicon = Lexicon.getDefaultLexicon();
		NLGFactory nlgFactory = new NLGFactory(lexicon);
		Realiser realiser = new Realiser(lexicon);
		LinkedList<String> forms = new LinkedList<String>();
		VPPhraseSpec v = nlgFactory.createVerbPhrase(verb);
		v.setFeature(Feature.TENSE, Tense.PAST);
		v.setFeature(Feature.PERSON, Person.THIRD);
		v.setFeature(Feature.NUMBER, NumberAgreement.PLURAL);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.PERSON, Person.FIRST);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.PERSON, Person.SECOND);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.TENSE, Tense.PRESENT);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.PERSON, Person.FIRST);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.PERSON, Person.THIRD);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.TENSE, Tense.FUTURE);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.PERSON, Person.SECOND);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.PERSON, Person.FIRST);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.NUMBER, NumberAgreement.SINGULAR);
		v.setFeature(Feature.PERSON, Person.FIRST);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.PERSON, Person.SECOND);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.TENSE, Tense.PRESENT);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.PERSON, Person.FIRST);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.PERSON, Person.THIRD);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.TENSE, Tense.FUTURE);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.PERSON, Person.SECOND);
		forms.add(realiser.realise(v).getRealisation());
		v.setFeature(Feature.PERSON, Person.FIRST);
		forms.add(realiser.realise(v).getRealisation());

		forms=removeDoubles(forms);
		return forms;
	}


	public static LinkedList<String> removeDoubles(LinkedList<String>list ){
		LinkedList<String> output = new LinkedList<String>();
		for (String s : list) {
			if (!output.contains(s)){
				output.add(s);
			}
		}
		return output;
	}

	}
