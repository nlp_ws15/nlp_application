package api;

import java.net.MalformedURLException;

import util.TextURL;

public class BritishOnlineCorpusAPI {

    public static int getNumberofHits(String ngram){
        TextURL url = null;
        try {
            ngram=ngram.replaceAll(" ","+");
            url = new TextURL("http://bnc.bl.uk/saraWeb.php?qy="+ ngram +"&mysubmit=Go");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String contents = url.read();
        if(contents.contains("Only ")) {
            String[] splitted = contents.split("Only ");
            splitted[0] = splitted[1].split(" ")[0];
            System.out.println(splitted[0]);
            int hits =Integer.parseInt(splitted[0]);
            return hits;
        } else if (contents.contains("from the ")) {
            String[] splitted = contents.split("from the ");
            splitted[0] = splitted[1].split(" ")[0];
            System.out.println(splitted[0]);
            int hits = Integer.parseInt(splitted[0]);
            return hits;
        } else {
        	return 0;
        }
    }
}
