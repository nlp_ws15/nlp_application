package api;

import java.net.URLEncoder;
import java.util.Map;
import java.util.TreeMap;

import org.jsoup.Jsoup;


public class GoogleAPI {
	
	public static Map<String, Long> resultHistory = new TreeMap<String, Long>();
	public static int numOfSearches = 0;
	public static final int totalNumOfSearches = 357;
	
	public static long getNumberOfResults(String searchString) throws Exception {
		searchString = "\"" + searchString + "\"";
		if(resultHistory.containsKey(searchString)) {
			return resultHistory.get(searchString);
		}
		String google = "http://www.google.com/search?q=";
		String charset = "UTF-8";
		//String userAgent = "ExampleBot 1.0 (+http://example.com/bot)"; // Change this to your company's name and bot homepage!
		String userAgent = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
		
		String totalResultsElementText = Jsoup.connect(google + URLEncoder.encode(searchString, charset)).userAgent(userAgent).get().getElementsByAttributeValue("id", "resultStats").text();
		// search has been executed
		numOfSearches ++;
		String totalResults = "";
		for(int i=0; i<totalResultsElementText.length(); i++) {
			char currentChar = totalResultsElementText.charAt(i);
			if(Character.isDigit(currentChar)) {
				totalResults = totalResults + currentChar;
			};
		}
		long totalResultsInt = Long.parseLong(totalResults);
		resultHistory.put(searchString, totalResultsInt);
		
		return totalResultsInt;
	}

	public int estimatedNumOfRemainingSearches() {
		return totalNumOfSearches - numOfSearches;
	}
	
}
