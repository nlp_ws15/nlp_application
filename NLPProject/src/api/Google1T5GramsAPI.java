package api;

import java.net.URLEncoder;
import java.util.Map;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Google1T5GramsAPI {
	public static Map<String, Long> resultHistory = new TreeMap<String, Long>();
	public static int numOfSearches = 0;
	
	public static long getNumberOfResults(String searchString) throws Exception {
		if(resultHistory.containsKey(searchString)) {
			return resultHistory.get(searchString);
		}
		String google = "http://corpora.linguistik.uni-erlangen.de/demos/cgi-bin/Web1T5/Web1T5_freq.perl?query=" + searchString + "&mode=Search&limit=50&threshold=100&optimize=on&wildcards=listed+normally&fixed=shown&.cgifields=debug&.cgifields=optimize";
		String charset = "UTF-8";
		//String userAgent = "ExampleBot 1.0 (+http://example.com/bot)"; // Change this to your company's name and bot homepage!
		// String userAgent = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
		Document doc = Jsoup.connect(google).get();
		//String totalResultsElementText = Jsoup.connect(google + URLEncoder.encode(searchString, charset)).userAgent(userAgent).get().getElementsByAttributeValue("id", "resultStats").text();
		// search has been executed
		/*
		numOfSearches ++;
		String totalResults = "";
		for(int i=0; i<totalResultsElementText.length(); i++) {
			char currentChar = totalResultsElementText.charAt(i);
			if(Character.isDigit(currentChar)) {
				totalResults = totalResults + currentChar;
			};
		}
		long totalResultsInt = Long.parseLong(totalResults);
		resultHistory.put(searchString, totalResultsInt);
		
		return totalResultsInt;
		*/
		return 0;
	}
}
