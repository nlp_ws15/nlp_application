package api;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;


public class TaggingAPI {
	private static MaxentTagger tagger; 
	
	public static void load() {
		tagger = new MaxentTagger("./models/english-left3words-distsim.tagger");
	}
	
	public static String tagString(String text) {
		return tagger.tagString(text);
	}
	
	public static List<String> tagSentences(String path) throws FileNotFoundException {
		String a = "sample-input.txt";
		  String b="./models/english-left3words-distsim.tagger";
	    MaxentTagger tagger = new MaxentTagger(b);
	    List<List<HasWord>> sentences = MaxentTagger.tokenizeText(new BufferedReader(new FileReader(a)));
	    List<String> sentencesStr = new ArrayList<String>();
	    for (List<HasWord> sentence : sentences) {
	      List<TaggedWord> tSentence = tagger.tagSentence(sentence);
	      sentencesStr.add(Sentence.listToString(tSentence, false));
	    }
	    return sentencesStr;
	}

}
