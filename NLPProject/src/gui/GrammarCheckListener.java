package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import algorithm.AlgorithmExecutor;

public class GrammarCheckListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		//String[] sentence = GrammarCheckerLayout.InputSentence.getText().split(" ");
		String sentence = GrammarCheckerLayout.InputSentence.getText();
		try {
			String[] sentences = AlgorithmExecutor.execute(sentence);
			GrammarCheckerLayout.showResult(sentences);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
