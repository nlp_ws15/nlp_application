/*
 * GridBagLayoutDemo.java requires no other files.
 */

package gui;

import java.awt.*;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

public class GrammarCheckerLayout {
    final static boolean shouldFill = true;
    final static boolean shouldWeightX = true;
    final static boolean RIGHT_TO_LEFT = false;
    static JFrame frame = new JFrame("Spellchecker");
    static TextField InputSentence = new TextField();
   
    public static JTextPane createResult(String[] sentence, int mistake)
    {
	    StyleContext context = new StyleContext();
	    StyledDocument document = new DefaultStyledDocument(context);
	
	
	    
	    SimpleAttributeSet styleIncorrectWords = new SimpleAttributeSet();
	    StyleConstants.setForeground(styleIncorrectWords, Color.RED);    
	    StyleConstants.setBold(styleIncorrectWords, true);
	    
	    SimpleAttributeSet styleCorrectWords = new SimpleAttributeSet();  
	    StyleConstants.setBold(styleCorrectWords, true);
    

	    try {
	    	for (int i = 0;i < sentence.length;i++)
	        {
	        	if(i == mistake)
	        	{
	        		document.insertString(document.getLength(), sentence[i] + " ", styleIncorrectWords);
	        	}
	        	else
	        	{
	        		document.insertString(document.getLength(), sentence[i] + " ", styleCorrectWords);
	        	}
	        	
	        }
	     
	      
	    } catch (BadLocationException badLocationException) {
	      System.err.println("Oops");
	    }
	
	    JTextPane textPane = new JTextPane(document);
	    textPane.setEditable(false);
	
	    
	    return textPane;
    }
    
    
    public static void addComponentsToPane(Container pane) {
        if (RIGHT_TO_LEFT) {
            pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }
        JButton button;
        GridBagConstraints c = new GridBagConstraints();
		//pane.setLayout(new GridBagLayout());
		//pane.setLayout(new  );
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		if (shouldFill) {	
			c.fill = GridBagConstraints.HORIZONTAL;
		}
		
			
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		pane.add(InputSentence, c);
	
	
	
		button = new JButton("Check");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 0;
		button.addActionListener(new GrammarCheckListener());
		pane.add(button, c);
    }
    
    public static void showResult(String[] sentence)
    {
    	
    	GridBagConstraints c = new GridBagConstraints();
    	c.fill = GridBagConstraints.HORIZONTAL;
    	c.ipady = 0;       //reset to default
    	c.weighty = 1.0;   //request any extra vertical space
    	c.anchor = GridBagConstraints.PAGE_END; //bottom of space
    	c.insets = new Insets(10,0,0,0);  //top padding
    	c.gridx = 0;       //aligned with button 2
    	c.gridwidth = 2;   //2 columns wide
    	c.gridy = 1;       //third row
    	//String[] text = {"I", "are", "a","young", "man"};
    	frame.getContentPane().add(createResult(sentence,3), c);
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Set up the content pane.
        addComponentsToPane(frame.getContentPane());

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
    
    public static void execute() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	createAndShowGUI();
            }
        });
	}
}
