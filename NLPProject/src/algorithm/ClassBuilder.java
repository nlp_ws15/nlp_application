package algorithm;

import java.util.LinkedList;
import java.util.List;

import api.VerbformsAPI;
import context.Context;
import util.Sentence;
import util.SentenceTreeSet;
import util.WordOfInterest;

public class ClassBuilder {
	/**
	 * Build a WordClass using a given tag type.
	 * 
	 * @param verb
	 * 		Currently only verbs are supported.
	 * @return
	 * 		The word class.
	 */
	public static void addClass(WordOfInterest basicWordOfInterest) {
		Context context = Context.INSTANCE;
		String currentSentence = context.getCurrentSentence();
		LinkedList<String> modifiedForms = VerbformsAPI.getOtherForms(basicWordOfInterest.getWord(), basicWordOfInterest.getType().toString());
		SentenceTreeSet<Sentence> sentences = new SentenceTreeSet<Sentence>();
		
		for(String modifiedForm : modifiedForms) {
			String modifiedSentence = currentSentence.replace(basicWordOfInterest.getWord(), modifiedForm);
			// the tag type of the original word remains unchanged
			Sentence sentenceWithClass = new Sentence(modifiedSentence, new WordOfInterest(modifiedForm, basicWordOfInterest.getType()));
			sentences.add(sentenceWithClass);
		}
		context.addSentencesWithProbabilites(sentences);
	}
}
