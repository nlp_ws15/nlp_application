package algorithm;

import api.BritishOnlineCorpusAPI;

public class CorrectnessChecker {
	public static boolean checkCorrectness(String sentence) {
		int numOfHits = BritishOnlineCorpusAPI.getNumberofHits(sentence);
		return numOfHits>0;
	}
}
