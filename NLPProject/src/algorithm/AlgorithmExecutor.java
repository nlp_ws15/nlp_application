package algorithm;

import java.util.List;
import context.Context;
import util.Sentence;
import util.SentenceTreeSet;
import util.WordOfInterest;

public class AlgorithmExecutor {
	public static String[] execute(String userInput) throws Exception {
		SentenceTreeSet<Sentence> sentences = new SentenceTreeSet<Sentence>();
		Context context = Context.INSTANCE;
		if(!CorrectnessChecker.checkCorrectness(userInput)) {
			context.setCurrentSentence(userInput);
			List<WordOfInterest> words = TaggerAccessor.getWordsWithType(TagType.getVerbalTypes());
			for(WordOfInterest word : words) {
				ClassBuilder.addClass(word);
			}
			sentences = ProbabilityEvaluator.getSentencesByProbabilityAsSentenceObject();
			for(Sentence sentence : sentences) {
				sentence.print();
			}
		} else {
			System.out.println("The sentence was correct");
		}
		//return sentences.toStringArray();
		// all context information needs to be deleted.
		context.close();
		return sentences.first().toString().split(" ");
	}
}
