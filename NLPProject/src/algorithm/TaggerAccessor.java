package algorithm;

import java.util.LinkedList;
import java.util.List;

import api.TaggingAPI;
import context.Context;
import util.WordOfInterest;

public class TaggerAccessor {
	
	public static List<WordOfInterest> getWordsWithType(List<TagType> tagTypes) {
		List<WordOfInterest> words = new LinkedList<WordOfInterest>();
		for(TagType tagType : tagTypes) {
			words.addAll(getWordsWithType(tagType));
		}
		return words;
	}
	/**
	 * Use the tagged sentence and return all words with a certain type.
	 * 
	 * @param tagType
	 * @return
	 */
	public static List<WordOfInterest> getWordsWithType(TagType tagType) {
		Context context = Context.INSTANCE;
		if(context.getTaggedSentence()==null) {
			context.setTaggedSentence(TaggingAPI.tagString(context.getCurrentSentence()));
		}
		String sentence = context.getTaggedSentence();
		
		List<WordOfInterest> words = new LinkedList<WordOfInterest>();
		String tagTypeString = tagType.toString();
		while(sentence.indexOf(tagTypeString)>-1) {
			StringBuilder word = new StringBuilder();
			for(int i = sentence.indexOf(tagTypeString)-2; i>0 && !Character.isWhitespace(sentence.charAt(i)); i--) {
				word.append(sentence.charAt(i));
			}
			word.reverse();
			sentence = sentence.substring(sentence.indexOf(tagTypeString)+tagTypeString.length());
			words.add(new WordOfInterest(word.toString(), tagType));
		}
		return words;
	}
}
