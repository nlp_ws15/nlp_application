package algorithm;

import java.util.LinkedList;
import java.util.List;

public enum TagType {
	CC,CD,DT,EX,FW,IN,JJ,JJR,JJS,LS,MD,NN,NNS,NNP,NNPS,PDT,POS,PRP,PRPS,RB,RBR,RBS,RP,SYM,TO,UH,VB,VBD,VBG,VBN,VBP,VBZ,WDT,WP,WPS,WRB;
	
	public static boolean contains(String tag) {
		try {
			TagType.valueOf(tag);
		} catch (IllegalArgumentException e) {
			return false;
		}
		return true;
	}
	
	public static List<TagType> getVerbalTypes() {
		List<TagType> verbalTypes = new LinkedList<TagType>();
		verbalTypes.add(VB);
		verbalTypes.add(VBD);
		verbalTypes.add(VBG);
		verbalTypes.add(VBN);
		verbalTypes.add(VBP);
		verbalTypes.add(VBZ);
		return verbalTypes;
	}
}
