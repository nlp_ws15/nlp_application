package algorithm;

import util.WordOfInterest;

public interface ISentence {
	public long getNumberOfHits();
	public WordOfInterest getWordClass();
	//public boolean equals(Object other);
}
