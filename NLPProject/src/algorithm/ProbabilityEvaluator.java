package algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import api.SearchAPI;
import context.Context;
import util.Sentence;
import util.SentenceTreeSet;
import util.WordOfInterest;

public class ProbabilityEvaluator {
	public static WordOfInterest getMostLikelyWordClass(List<WordOfInterest> wordClasses) {
		/*
		WordClass mostLikelyWordClass = wordClasses.get(0);
		for(WordClass wordClass : wordClasses) {
			if(wordClass.getProbability()>mostLikelyWordClass.getProbability()) {
				mostLikelyWordClass = wordClass;
			};
		}
		return mostLikelyWordClass;
		*/
		return null;
	}
	
	public static Set<String> getSentencesByProbabilityAsString() throws Exception {
		
		SentenceTreeSet<Sentence> sentences = getSentencesByProbability();
		return sentences.getClassOfMostLikelyErrorAsString();
	}
	
	public static SentenceTreeSet<Sentence> getSentencesByProbabilityAsSentenceObject() throws Exception {
		return getSentencesByProbability();
	}
	
	private static SentenceTreeSet<Sentence> getSentencesByProbability() throws Exception {
		Context context = Context.INSTANCE;
		SentenceTreeSet<Sentence> sentences = context.getSentencesWithProbabilities();
		for(Sentence sentence : sentences) {
			long numberOfHits = SearchAPI.getNumberOfResults(sentence.toString());
			sentence.setNumOfHits(numberOfHits);
		}
		long totalNumberOfHits = sentences.getTotalNumberOfHits();
		for(Sentence sentence : sentences) {
			sentence.setRelativeProbabilty(totalNumberOfHits);
		}
		sentences.updateAll();
		return sentences;
	}
}
