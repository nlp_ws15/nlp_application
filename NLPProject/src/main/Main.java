package main;

import algorithm.AlgorithmExecutor;
import api.Google1T5GramsAPI;
import api.GoogleAPI;
import api.TaggingAPI;
import demo.TaggerDemo;
import gui.GrammarCheckerLayout;

public class Main {
	public static void main(String[] args) {
		try {
			//Google1T5GramsAPI.getNumberOfResults("I am a man");
			// System.out.println(googleAPI.getNumberOfResults("dance"));
			// TaggerDemo.executeTaggerDemo();
			//GrammarCheckerLayout.execute();
			// AlgorithmExecutor.execute("I is a man");
			TaggingAPI.load();
			GrammarCheckerLayout.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
