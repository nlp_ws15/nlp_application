package util;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import algorithm.ISentence;

public class SentenceTreeSet<E extends ISentence> extends TreeSet<E> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9026762165749577201L;


	public boolean add(E sentence) {
		return super.add(sentence);
	}
	
	public boolean updateAll() {
		Set<E> tempSet = new TreeSet<E>();
		boolean successful = true;
		for(E sentence : this) {
			tempSet.add(sentence);
		}
		this.clear();
		
		if(!this.addAll(tempSet)) {
			successful = false;
		};
		return successful;
	}
	
	
	/**
	 * Return a subset that contains the class with the most likely error.
	 * @return
	 * 		the sorted subset.
	 */
	public Set<Sentence> getClassOfMostLikelyError() {
		updateAll();
		WordOfInterest mostLikelyError = ((Sentence)super.first()).getWordClass();
		TreeSet<Sentence> mostLikelyClassSentences = new TreeSet<Sentence>();
		while(iterator().hasNext()) {
			Sentence currentSent = (Sentence)this.iterator().next();
			if(currentSent.getWordClass().equals(mostLikelyError)) {
				mostLikelyClassSentences.add(currentSent);
			}
		}
		return mostLikelyClassSentences;
	}
	
	public Set<String> getClassOfMostLikelyErrorAsString() {
		updateAll();
		WordOfInterest mostLikelyError = super.first().getWordClass();
		Set<String> mostLikelyClassSentences = new TreeSet<String>();
		while(iterator().hasNext()) {
			E currentSent = iterator().next();
			if(currentSent.getWordClass().equals(mostLikelyError)) {
				mostLikelyClassSentences.add(currentSent.toString());
			}
		}
		return mostLikelyClassSentences;
	}

	public long getTotalNumberOfHits() {
		long totalNumberOfHits = 0;
		for(E sentence : this) {
			totalNumberOfHits = totalNumberOfHits + sentence.getNumberOfHits();
		}
		return totalNumberOfHits;
	}
	
	public String[] toStringArray() {
		String[] asStringArray = new String[size()];
		Iterator<E> it = this.iterator();
		for(int i=0; i<size(); i++) {
			asStringArray[i] = it.next().toString();
		}
		return asStringArray;
	}
}
