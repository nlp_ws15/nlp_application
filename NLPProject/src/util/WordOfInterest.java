package util;

import algorithm.TagType;

public class WordOfInterest {
	
	private String word;
	private TagType classType;
	
	public WordOfInterest(String word, TagType classType) {
		this.word = word;
		this.classType = classType;
	}
	
	public void setWord(String word) {
		this.word = word;
	}
	
	public void setClassType(TagType classType) {
		this.classType = classType;
	}
	
	public String getWord() {
		return word;
	}
	
	public int getProbability() {
		return 1;
	}

	public TagType getType() {
		return classType;
	}
}
