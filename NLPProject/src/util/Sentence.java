package util;

import algorithm.ISentence;

public class Sentence implements Comparable<Sentence>, ISentence {
	private String sentence = "";
	private float relativeProbability = 0.f;
	private long numOfHits = 0;
	private WordOfInterest wordClass;
	
	public Sentence(String sentence, WordOfInterest wordClass) {
		this.sentence = sentence;
		this.wordClass = wordClass;
	}
	
	public void setRelativeProbabilty(long totalNumberOfHits) {
		this.relativeProbability = (float)numOfHits/(float)totalNumberOfHits;
	}
	
	public void setNumOfHits(long numOfHits) {
		this.numOfHits = numOfHits;
	}
	
	public float getAbsoluteProbability() {
		return wordClass.getProbability() * relativeProbability;
	}
	
	public WordOfInterest getWordClass() {
		return wordClass;
	}
	
	@Override
	public String toString() {
		return sentence;
	}

	@Override
	public int compareTo(Sentence other) {
		if(this.equals(other)) {
			return 0;
		}
		if(other.getAbsoluteProbability()>=this.getAbsoluteProbability()) {
			return 1;
		}
		return -1;
	}

	@Override
	public long getNumberOfHits() {
		return numOfHits;
	}
	
	@Override
	public boolean equals(Object other) {
		if(other instanceof Sentence) {
			Sentence otherSent = (Sentence)other;
			if(otherSent.sentence.equals(this.sentence)) {
				return true;
			}
		}
		return false;
	}

	public void print() {
		System.out.println("Probability: " + relativeProbability + ", Sentence: " + sentence);
	}
	
}
