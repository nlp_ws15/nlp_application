package demo;

import java.util.List;

import api.TaggingAPI;
import static algorithm.TagType.*;

public class TaggerDemo {

  public static void executeTaggerDemo() throws Exception {
	  TaggingAPI api = new TaggingAPI();
	  List<String> sentences = api.tagSentences("sample-input.txt");
	  //System.out.println("String to tag:" + api.tagString("I like watching movies"));
	  System.out.println("String to tag:" + api.tagString("I are a man"));
	  System.out.println("String to tag:" + api.tagString("I am a man"));
	  /*
	  List<String> words = api.getWordsWithType(api.tagString("I am a man"), VBP);
	  for(String word : words) {
		  System.out.println(word);
	  }
	  System.out.println("Sentences to tag: ");
	  for(String sentence : sentences) {
		  System.out.println(sentence);
	  }
	  */
  }

}
